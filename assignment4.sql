

CREATE TABLE dbo.Group5_Contacts
(
    [ContactID] int IDENTITY(1,1) PRIMARY KEY NOT NULL,				   -- PRIMARY KEY
    [ContactFirstName] varchar (35) NOT NULL,                          -- First Name
    [ContactLastName] varchar (35) NOT NULL,                           -- Last Name
    [ContactPhone] varchar (20) NOT NULL,                              -- Phone Number
    [ContactEmail] varchar (70) NULL,                                  -- Email
    [Active] bit DEFAULT(1) NOT NULL								   -- Active
)

CREATE TABLE dbo.Group5_Countries
(
    [CountryID] int IDENTITY(1,1) PRIMARY KEY NOT NULL,				-- PRIMARY KEY
    [CountryName] varchar(255) NOT NULL,                            -- Country Name
    [CountryCode] varchar(2) NOT NULL,                              -- Country Code
    [Active] bit DEFAULT(1) NOT NULL								-- Active
)

CREATE TABLE dbo.Group5_Sponsors
(
    [SponsorID] int IDENTITY(1,1) PRIMARY KEY NOT NULL,				   -- PRIMARY KEY
    [SponsorName] varchar (200) NOT NULL,                              -- Name
    [SponsorPhone] varchar (20) NULL,                                  -- Phone Number
    [SponsorEmail] varchar (70) NULL,                                  -- Email
    [ContactID] int FOREIGN KEY REFERENCES dbo.Group5_Contacts NOT NULL,  -- Key - Primary Contact
    [Active] bit DEFAULT(1) NOT NULL								   -- Active
)


CREATE TABLE dbo.Group5_Genres
(
    [GenreID] int IDENTITY(1,1) PRIMARY KEY NOT NULL,         -- PRIMARY KEY
    [GenreName] varchar(200) NOT NULL,					      --Genre Name
    [Active] bit DEFAULT(1) NOT NULL                          -- Active
)
CREATE TABLE dbo.Group5_Games
(
    [GameID] int IDENTITY(1,1) PRIMARY KEY NOT NULL,                    -- PRIMARY KEY
    [GameName] varchar(200) NOT NULL,									--Game Name
    [GenreID] int FOREIGN KEY REFERENCES dbo.Group5_Genres NOT NULL,    -- FKEY
    [Active] bit DEFAULT(1) NOT NULL                                    -- Active
)

CREATE TABLE dbo.Group5_Teams
(
    [TeamID] int IDENTITY(1,1) PRIMARY KEY NOT NULL,                     -- PRIMARY KEY
    [CountryID] int FOREIGN KEY REFERENCES dbo.Group5_Countries NOT NULL, -- FKey
    [TeamName] varchar(200) NOT NULL,									--Team Name
    [GameID] int FOREIGN KEY REFERENCES dbo.Group5_Games NOT NULL,		-- FKEY - Game ID
    [Active] bit DEFAULT(1) NOT NULL                                     -- Active
)


CREATE TABLE dbo.Group5_Gender
(
    [GenderID] int IDENTITY(1,1) PRIMARY KEY NOT NULL,					-- PRIMARY KEY
    [GenderName] varchar(200) NOT NULL,									--Gender Name
)

CREATE TABLE dbo.Group5_Athletes
(
    [AthleteID] int IDENTITY(1,1) PRIMARY KEY NOT NULL,				   -- PRIMARY KEY
    [AthleteFirstName] varchar (200) NOT NULL,                         -- First Name
    [AthleteLastName] varchar (200) NOT NULL,                          -- Last Name
    [AthletePhone] varchar (30) NULL,                                  -- Phone Number
    [AthleteEmail] varchar (200) NULL,                                 -- Email
    [AthleteAlias] varchar (70) NULL,                                  -- Gamer Alias
    [AthleteCity] varchar (200) NULL,                                  -- Gamer Alias
    [AthleteDOB] Date NOT NULL,
    [GenderID] int FOREIGN KEY REFERENCES dbo.Group5_Gender NOT NULL, -- FKEY
    [CountryID] int FOREIGN KEY REFERENCES dbo.Group5_Countries NOT NULL, -- FKEY
    [Active] bit DEFAULT(1) NOT NULL								   -- Active
)


CREATE TABLE dbo.Group5_Leagues
(
    [LeagueID] int IDENTITY(1,1) PRIMARY KEY NOT NULL,                  -- PRIMARY KEY
    [LeagueName] varchar(250) NOT NULL,                                 --League Name
    [GameID]  int FOREIGN KEY REFERENCES dbo.Group5_Games NOT NULL,     -- FKEY - Game ID
    [StartDate] Date NOT NULL,											-- Start Date
    [EndData] Date NULL,												-- End Date
    [Active] bit DEFAULT(1) NOT NULL                                    -- Active
)

CREATE TABLE dbo.Group5_Matches
(
    [MatchID] int IDENTITY(1,1) PRIMARY KEY NOT NULL,                -- PRIMARY KEY
    [MatchDate] Date NOT NULL,                                       -- Date of match
    [Active] bit DEFAULT(1) NOT NULL                                 -- Active
)

/*
 *  #######  Association Tables ########
*/


CREATE TABLE dbo.Group5_SponsorsTeams
(
    [SponsorTeamID] int IDENTITY(1,1) PRIMARY KEY NOT NULL,				-- PRIMARY KEY
    [SponsorID] int FOREIGN KEY REFERENCES dbo.Group5_Sponsors NOT NULL,-- FKEY Sponsor
    [TeamID] int FOREIGN KEY REFERENCES dbo.Group5_Teams     NOT NULL,  -- FKEY Team
    [SponsorTeamStartDate] Date NOT NULL,                               -- Start Date
    [SponsorTeamEndData] Date NULL                                      -- End   Date
)

CREATE TABLE dbo.Group5_AthletesTeams
(
    [AthletesTeamsID] int IDENTITY(1,1) PRIMARY KEY NOT NULL,            -- PRIMARY KEY
    [TeamID] int FOREIGN KEY REFERENCES dbo.Group5_Teams NOT NULL,       -- FKEY
    [AthleteID] int FOREIGN KEY REFERENCES dbo.Group5_Athletes NOT NULL, -- FKEY
    [AthleteTeamStartDate] Date NOT NULL,                                -- Start Date
    [AthleteTeamEndData] Date NULL                                       -- End Date
)


CREATE TABLE dbo.Group5_MatchesTeams
(
    [MatchesTeamsID] int IDENTITY(1,1) PRIMARY KEY NOT NULL,				 -- PRIMARY KEY
    [MatchID] int FOREIGN KEY REFERENCES dbo.Group5_Matches NOT NULL,        -- FKEY
    [TeamID] int FOREIGN KEY REFERENCES dbo.Group5_Teams NOT NULL,           -- FKEY
    [MatchTeamWinner] bit DEFAULT(1) NOT NULL                                -- Active
)


CREATE TABLE dbo.Group5_TeamsLeagues
(
    [TeamsLeaguesID] int IDENTITY(1,1) PRIMARY KEY NOT NULL,                 -- PRIMARY KEY
    [TeamID] int FOREIGN KEY REFERENCES dbo.Group5_Teams NOT NULL,           -- FKEY
    [LeagueID] int FOREIGN KEY REFERENCES dbo.Group5_Leagues NOT NULL        -- FKEY
)




/* Drop Table Scripts
DROP TABLE dbo.Group5_Contacts
DROP TABLE dbo.Group5_Countries
DROP TABLE dbo.Group5_Sponsers
DROP TABLE dbo.Group5_SponsersTeams
DROP TABLE dbo.Group5_Teams
DROP TABLE dbo.Group5_Games
DROP TABLE dbo.Group5_Genres
DROP TABLE dbo.Group5_Gender
DROP TABLE dbo.Group5_Athletes
DROP TABLE dbo.Group5_AthletesTeams
DROP TABLE dbo.Group5_Leauges
DROP TABLE dbo.Group5_Matches
DROP TABLE dbo.Group5_MatchesTeams
DROP TABLE dbo.Group5_TeamLeagues
*/

