/*
Dane Wikstrom
Assignment 5
CS 3550
*/


USE WideWorldImporters



/*
Question #1 - 10 points

Without adding any additional tables to the FROM statement below,
	add the column "PackageTypeName" (from Warehouse.PackageTypes) to the
	results
*/

DECLARE @packageTypeName varchar(200) 
SELECT @packageTypeName = Warehouse.PackageTypes.PackageTypeName FROM Warehouse.PackageTypes

SELECT
	IL.PackageTypeID,
	SUM(IL.ExtendedPrice),
	@packageTypeName AS [Package Type Name]
FROM
	Sales.Invoices I
	INNER JOIN Sales.InvoiceLines IL 
	ON I.InvoiceID = IL.InvoiceID

GROUP BY
	IL.PackageTypeID


/*
Question #2 - 20 points

Write a query that returns each salesperson's best year and worst year, 
	based on the Invoice tables.  Use the ExtendedPrice column from the 
	InvoiceLines table for each line's contribution (I don't want you 
	to calculate the contribution as we've done in other queries).
	Order the results by the lowest of the worst years first.

You are not allowed to use a union, case statement, or windowed function. 

You will get 10 rows back - first and last records look like the following:

14	Lily Code	2422204.13	6574146.27
15	Taj Shand	2896948.01	6045415.13

*/

SELECT
	P.PersonID[ID],
	P.FullName[Full Name],
	MIN(Total.TotalSales) [Worst Year],
	MAX(Total.TotalSales) [Best Year]
	
FROM
	Sales.Invoices I
	INNER JOIN Sales.InvoiceLines IL ON I.InvoiceID = IL.InvoiceID
	INNER JOIN Application.People P ON P.PersonID = I.SalespersonPersonID
	INNER JOIN
		(
		SELECT
			SUM(IL.ExtendedPrice)[TotalSales], 
			P.PersonID [Person ID]
		FROM
			Sales.Invoices I
			INNER JOIN Sales.InvoiceLines IL ON I.InvoiceID = IL.InvoiceID
			INNER JOIN Application.People P ON P.PersonID = I.SalespersonPersonID
		GROUP BY
			P.PersonID,
			YEAR(I.InvoiceDate)
		)Total ON P.PersonID = Total.[Person ID]
GROUP BY
	P.PersonID,
	P.FullName
ORDER BY
	[Worst Year]

/*
Question #3 - 20 points

The sales manager wants to look at the current year (2016 in this case) sales 
	compared to the lifetime sales for each salesperson.  This is calculated by
	aggregating the sales for 2016 (based on the Invoice tables) divided by
	the sum of all sales for all time.  Order the list in descending order of the 
	% of life time sales that 2016 represents.  Your query will return 10 records 
	and the top two results look like:

PersonID	FullName		2016Sales	%OfLifeTimeSales
15			Taj Shand		2896948.01	0.145703
16			Archer Lamble	2850011.20	0.138150

You are not allowed to use a union, case statement, or windowed function. 
*/

SELECT
	P.PersonID [ID],
	P.FullName [Full Name],
	SalesFor2016.[2016 Sales],
	SalesFor2016.[2016 Sales] / PerOfLifeTimeSales.[LifeTimeSales]
FROM
	Sales.Invoices I
	INNER JOIN Sales.InvoiceLines IL ON I.InvoiceID = IL.InvoiceID
	INNER JOIN Application.People P ON P.PersonID = I.SalespersonPersonID
	INNER JOIN
	(
	SELECT
		P.PersonID [Person ID],
		SUM(IL.ExtendedPrice) [2016 Sales]
	FROM
		Sales.Invoices I
		INNER JOIN Sales.InvoiceLines IL ON I.InvoiceID = IL.InvoiceID
		INNER JOIN Application.People P ON P.PersonID = I.SalespersonPersonID
	WHERE
		YEAR(I.InvoiceDate) IN (2016)
	GROUP BY
		P.PersonID
	) SalesFor2016 ON P.PersonID = SalesFor2016.[Person ID]

	INNER JOIN
	(
	SELECT
		P.PersonID [Person ID],
		SUM(IL.ExtendedPrice) [LifeTimeSales]
	FROM
		Sales.Invoices I
		INNER JOIN Sales.InvoiceLines IL ON I.InvoiceID = IL.InvoiceID
		INNER JOIN Application.People P ON P.PersonID = I.SalespersonPersonID
	GROUP BY
		P.PersonID
	) PerOfLifeTimeSales ON P.PersonID = PerOfLifeTimeSales.[Person ID]

GROUP BY
	P.PersonID,
	P.FullName,
	SalesFor2016.[2016 Sales],
	PerOfLifeTimeSales.[LifeTimeSales]
ORDER BY
	[2016 Sales] DESC