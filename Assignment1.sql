--Dane Wikstrom
--Assignment 1

USE WideWorldImporters

/*
Instructions - You will be using the WideWorldImporters database
	to complete this assignment.  Points for each question will be listed
	as part of the question.  You will get 60% of the points for getting the correct
	answer, 20% of the points for using good query formatting (example provided 
	and discussed in class), and 20% of the points for using a reasonable 
	approach.  You will turn in this file with the completed queries. 
*/

/*
Question #1 - (5 points) Write a "Hello World" query.  I'd like you to create a select
	statement that returns "Hello World, my name is <your name here>"

	Your output will look similiar to this - 	
	Hello World, my name is Russell Reed

*/

SELECT 
	'Hello World, my name is Dane Wikstrom' 
	AS 'Hello World!'

/*
Question #2 - (5 points) Write a query to return all records from the
	Application.People table.  Include the PersonId, FullName,
	PreferredName, EmailAddress, and IsSalesPerson columns

	An example of one of the records returned should look like the following (including header):
	PersonId	FullName		PreferredName	EmailAddress					IsSalesperson
	2			Kayla Woodcock	Kayla			kaylaw@wideworldimporters.com	1
*/

SELECT 
	P.PersonID, 
	P.FullName, 
	P.PreferredName, 
	P.EmailAddress, 
	P.IsSalesperson
FROM
	Application.People P
	
/* 
Question #3 (5 points)- Write a query that returns all the Orders for CustomerID 33
	(Sales.Orders table) for the year 2016.  Return the Order ID, the Customer ID,
	Salesperson ID, and the Order Date
	
	You should get 14 records back (assuming I've not mucked up my database).
*/

SELECT 
	SO.OrderID, 
	SO.CustomerID, 
	SO.SalespersonPersonID, 
	SO.OrderDate
FROM 
	Sales.Orders SO
WHERE
	So.CustomerID = 33
	AND
	OrderDate BETWEEN '2016-01-01' AND '2016-12-31'

/* 
Question #4 (5 points)- Write a query that returns a list of cities inside of Utah.
	This information can be found in the tables Application.Cities and
	Application.StateProvinces (joined by StateProvinceID.  Return the cityID,
	the city name, and the state name ("Utah").  Order the list by City name

	Do not hard code the state value - join the two tables together.  You should get
	399 records.  First city is Abraham, last one is Zane
*/

SELECT
	C.CityID, 
	C.CityName, 
	SP.StateProvinceName
FROM
	Application.Cities C 
	INNER JOIN Application.StateProvinces SP
	ON C.StateProvinceID = SP.StateProvinceID
WHERE
	SP.StateProvinceName = 'Utah'
ORDER BY
	C.CityName

/*
Question #5 (10 points) - Write a query that returns all the "Novelty Items" carried by
	the supplier "Northwind Electric Cars".  I'd like the stock item id,
	stock item name, stock group name, brand, size, recommended retail price,
	and supplier name.  Order the final list with the cheapest item first.

	You should get 18 records back.  Cheapest item is "RC toy sedan car with remote control (Black) 1/50 scale"
	at 37.38. 
*/

SELECT
	SI.StockItemID, 
	SI.StockItemName, 
	SG.StockGroupName, 
	SI.Brand, 
	SI.Size, 
	SI.RecommendedRetailPrice, 
	S.SupplierName
FROM 
	Warehouse.StockItems SI, 
	Warehouse.StockGroups SG, 
	Purchasing.Suppliers S
WHERE
	S.SupplierName = 'Northwind Electric Cars'
	AND
	SG.StockGroupName = 'Novelty Items'
	AND
	SI.Brand IS NOT NULL
ORDER BY
	SI.RecommendedRetailPrice ASC
