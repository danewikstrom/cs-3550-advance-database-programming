CREATE TABLE NationalParks
(
	NationalParkID int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	NationalParkName varchar(30) NOT NULL,
	NationalParkAdmissionPrice money,
	NationalParkArea varchar(30) NOT NULL,
	NationalParkDescription varchar(500)
)

CREATE TABLE NationalParkActivities
(
	NationalParkActivitiesID int IDENTITY(1,1) NOT NULL,
	NationalParkID int FOREIGN KEY REFERENCES NationalParks(NationalParkID) NOT NULL,
	NationalParkActivitiesNAme varchar(30) NOT NULL,
	NationalParkActivitiesActive bit NOT NULL,
	NationalParkActivitiesReservationNeeded bit NOT NULL
)

CREATE TABLE NationalParksAddresses
(
	NationalParkAddressID int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	NationalParkID int FOREIGN KEY REFERENCES NationalParks(NationalParkID) NOT NULL,
	NationalParkAddressLine1 varchar(30) NOT NULL,
	NationalParkAddressLine2 varchar(30) NOT NULL,
	NationalParkCity varchar(15) NOT NULL,
	NationalParkState char(2) NOT NULL,
	NationalParkZip varchar(15) NOT NULL
)

CREATE TABLE NationalParkContacts
(
	NationalParkContactID int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	NationalParkID int FOREIGN KEY REFERENCES NationalParks(NationalParkID) NOT NULL,
	NationalParkContactFirstName varchar(20) NOT NULL,
	NationalParkContactLastName varchar(30) NOT NULL,
	NationalParkContactPhoneNumber varchar(30) NOT NULL,
	NationalParkContactTitle varchar(30) NOT NULL,
	NationalParkContactEmail varchar(30) NOT NULL
)

CREATE TABLE Campgrounds
(
	CampgroundID int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	NationalParkID int FOREIGN KEY REFERENCES NationalParks(NationalParkID) NOT NULL,
	CampgroundName varchar(30) NOT NULL,
	CampgroundDescription varchar(300),
	CampgroundHoursOfOperation varchar(150),
)

CREATE TABLE CampgroundAddressses
(
	CampgroundAddressID int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	CampgroundID int FOREIGN KEY REFERENCES Campgrounds(CampgroundID) NOT NULL,
	CampgroundAddressLine1 varchar(30) NOT NULL,
	CampgroundAddressLine2 varchar(30) NOT NULL,
	CampgroundCity varchar(15) NOT NULL,
	CampgroundState char(2) NOT NULL,
	CampgroundZip varchar(15) NOT NULL
)

CREATE TABLE CampgroundContacts
(
	CampgroundContactID int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	CampgroundID int FOREIGN KEY REFERENCES Campgrounds(CampgroundID) NOT NULL,
	CampgroundContactFirstName varchar(20) NOT NULL,
	CampgroundContactLastName varchar(30) NOT NULL,
	CampgroundContactPhoneNumber varchar(30) NOT NULL,
	CampgroundContactTitle varchar(30) NOT NULL,
	CampgroundContactEmail varchar(30) NOT NULL
)

CREATE TABLE CampgroundActivities
(
	CampgroundActivityID int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	CampgroundID int FOREIGN KEY REFERENCES Campgrounds(CampgroundID) NOT NULL,
	CampgroundActivityName varchar(20) NOT NULL
)

CREATE TABLE CampgroundAmenities
(
	CampgroundAmenityID int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	CampgroundID int FOREIGN KEY REFERENCES Campgrounds(CampgroundID) NOT NULL,
	CampgroundAmenityName varchar(30) NOT NULL,
	CampgroundAmenitiesActive bit NOT NULL,
	CampgroundAmenitieisReservationNeeded bit NOT NULL
)

CREATE TABLE CampgroundNaturalFeatures
(
	CampgroundNaturalFeatureID int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	CampgroundNaturalFeatureName varchar(30) NOT NULL,
	CampgroundNaturalFeatureDescription varchar(500)
)

CREATE TABLE CampgroundNaturalFeatureBridge
(
	CampgroundNaturalFeatureID int PRIMARY KEY NOT NULL,
	CampgroundID int FOREIGN KEY REFERENCES Campgrounds(CampgroundID) NOT NULL,
)