--Dane Wikstrom
--Assignment 2 RE-SUBMISSION
--CS 3550


USE WideWorldImporters

/*
Question #1 - 5 points

The marketing team wants a list of all "Computing Novelties" products for an
	email campaign they are putting together.  Write a query to return the 83 Stock
	Items that fall in this category.  Include the Stock Item ID, the name of the item,
	the supplier name, recommended price, color, stock group name, and supplier name.  If the Color is null
	then replace the value with an empty string ('').

This query will return 83 rows.  13 items will not have a color
*/

SELECT 
	SI.StockItemID AS 'Stock Item ID',
	SI.StockItemName AS 'Stock Item Name',
	SI.RecommendedRetailPrice AS 'Recommended Retail Price', 
	ISNULL(C.ColorName, '') AS 'Color',
	SG.StockGroupName AS 'Stock Group Name',
	S.SupplierName AS 'Supplier Name'
	
FROM
	Warehouse.StockItems SI
	INNER JOIN Purchasing.Suppliers S ON SI.SupplierID = S.SupplierID
	INNER JOIN Warehouse.StockItemStockGroups SSG ON SSG.StockItemID = SI.StockItemID
	INNER JOIN Warehouse.StockGroups SG ON SG.StockGroupID = SSG.StockGroupID
	LEFT JOIN Warehouse.Colors C ON SI.ColorID = C.ColorID
		
WHERE 
	SG.StockGroupName = 'Computing Novelties'


/*
Question #2 - 10 Points

Write a query that returns all the sales people from the Application.People
table.  The follow rules apply to this list:
	- A salesperson is someone with the isSalesperson flag set
	- Include the PersonID 
	- Fix the full name field to be last name then a comma and a space then the first name.  
		Assume that the current name only has a first and last name separated by a space
	- Do not show @wideworldimporters on the network account (only the part before it).
	- Remove all the parenthesis, dashes, and spaces from the phone number 
	
Kayla Woodcock's record would look as follows: 
PersonID	FullName			NetworkAccount	PhoneNumber		EmailAddress
2			Woodcock, Kayla		kaylaw			4155550102		kaylaw@wideworldimporters.com
*/

SELECT
	P.PersonID,
	LTRIM(SUBSTRING(P.FullName, CHARINDEX(' ', P.FullName), LEN(P.FullName)) ) + ', ' + 
		LEFT(P.FullName, CHARINDEX(' ', P.FullName) ) AS 'Full Name',
	LEFT(P.LogonName, CHARINDEX('@', P.LogonName) - 1) AS 'Network Account',
	REPLACE(REPLACE(REPLACE(REPLACE(P.PhoneNumber,')',''),'(',''),'-',''),' ','') AS 'Phone Number',
	P.EmailAddress AS 'Email Address'
FROM
	Application.People P
WHERE
	P.IsSalesperson = 1

/*
Question #3 - 10 points

Its time for the annual trade show and you want to make sure all
	your suppliers make it to the party.  The marketing team needs
	a list of all your suppliers, their full delivery address, and 
	the name of both the primary and alternate contacts for the 
	supplier. Order the results by supplier name

	You'll be using a number of tables - Purchasing.Suppliers, and
	the People, Cities, and StateProvinces tables in the Application 
	schema.  The columns to join things up are a bit more tricky - 
		- Suppliers to people on the PersonID and the PrimaryContactPersonID
			or AlternateContactPersonID.
		- Cities on CityID to Suppliers on DeliveryCityID
		- StateProvices on StateProvinceID (on both sides)

	Something to keep in mind...  you can join to the same table multiple times
	as long as you give them different aliases.

This query will return 13 rows and 9 columns.  An example to compare to - 

SupplierName		PrimaryContactName	AlternateContactName	DeliveryAddressLine1	DeliveryAddressLine2		DeliveryCityID	CityName	StateProvinceCode	DeliveryPostalCode
A Datum Corporation	Reio Kabin			Oliver Kivi				Suite 10				183838 Southwest Boulevard	38171			Zionsville	IN					46077
*/


SELECT
	S.SupplierName,
	P.FullName AS 'PrimaryContactName',
	PE.FullName AS 'AlternateContactName',
	S.DeliveryAddressLine1,
	S.DeliveryAddressLine2,
	S.DeliveryCityID,
	C.CityName,
	SP.StateProvinceCode,
	S.DeliveryPostalCode

FROM
	Purchasing.Suppliers S
		INNER JOIN Application.People P 
		ON P.PersonID = S.PrimaryContactPersonID
		INNER JOIN Application.People PE
		ON PE.PersonID = S.AlternateContactPersonID--,
	INNER JOIN Application.Cities C
		ON C.CityID = S.DeliveryCityID
		JOIN Application.StateProvinces SP
		ON C.StateProvinceID = SP.StateProvinceID

ORDER BY
	S.SupplierName


/*
Question #4 - 10 points

Archer Lamble is preparing to send invites to the annual
	Novelty Shop Expo for his high achieving customers.
	To recieve an invitation, a novelty shop must have purchased
	more than 32k in 2013.  Archer only has 10 invitations - if more
	than 10 people have reached the 32k goal, the 10 highest sales
	get the invitation.

Utilizing Sales.Invoices, InvoiceLines, Sales.Customers, and 
	Sales.CustomerCategories tables, write a query that provides
	Archer his list.  You need to calulate the value of each invoice line by 
	multiplying quanity and unit price and then multiple this value by the 
	tax rate.  Do not use the precalculated fields - do the calculation yourself 
	(you can check extendedPrice to see if you got it correct for each invoice line).
	Return the customer name and how much they sold for 2013

If you run your query for 2014, you will get 10 records with the highest seller being
	Tailspin Toys (Cherry Grove Beach, SC) with 43372.020000 in revenue.  For 2013, you'll 
	only get 9 records with the highest being Wingtip Toys (Lake Ronkonkoma, NY) at 43522.900000
*/

SELECT TOP 10
	C.CustomerName,
	SUM((IL.Quantity*IL.UnitPrice)*(1 + (IL.TaxRate / 100))) [Total Sales for 2013]
FROM 
	Sales.Invoices I
	INNER JOIN Sales.InvoiceLines IL ON I.InvoiceID = IL.InvoiceID
	INNER JOIN Sales.Customers C ON C.CustomerID = I.CustomerID
	INNER JOIN Sales.CustomerCategories CC ON CC.CustomerCategoryID = C.CustomerCategoryID
	INNER JOIN Application.People P ON P.PersonID = I.SalespersonPersonID

WHERE 
	P.PersonID = 16 AND				--Archer's ID number
	CC.CustomerCategoryID = 3 AND	--CustomerCatergory ID for Novelty Shop
	YEAR(I.InvoiceDate) = 2013
	--YEAR(I.InvoiceDate) = 2014	--To test for 2014

GROUP BY
	C.CustomerName,
	CC.CustomerCategoryName
HAVING 
	SUM ( ( (IL.Quantity*IL.UnitPrice)*(1 + (IL.TaxRate / 100)) ) ) > 32000