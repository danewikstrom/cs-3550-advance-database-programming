/*
Dane Wikstrom
CS 3550
Final Part 3
*/



/* 
Write a query that creates a list of employees from the Application.People 
	table in WideWorldImporters (isEmployee = 1).  Your query will need
	to generate a JSON object for each employee in the below format.

Turn in the .SQL file for this query.

[
   {
      "PersonKey":2,
      "FirstName":"Kayla ",
      "LastName":"Woodcock",
      "Email":"kaylaw@wideworldimporters.com",
      "Phone":"(415) 555-0102",
      "CurrentJob":{
         "SalesPerson":"Yes",
         "Title":"Team Member",
         "Territory":"Plains",
         "HireDate":"2008-04-19T00:00:00"
      }
   },

....

   {
      "PersonKey":20,
      "FirstName":"Jack ",
      "LastName":"Potter",
      "Email":"jackp@wideworldimporters.com",
      "Phone":"(415) 555-0102",
      "CurrentJob":{
         "SalesPerson":"Yes",
         "Title":"General Manager",
         "Territory":"Southeast",
         "HireDate":"2009-05-29T00:00:00"
      }
   }
]

*/

USE WideWorldImporters

SELECT
	P.PersonID [PersonKey],
	LEFT(P.FullName, CHARINDEX(' ', P.FullName)) [FirstName],
	LTRIM(SUBSTRING(P.FullName, CHARINDEX(' ', P.FullName), LEN(P.FullName))) [LastName],
	P.EmailAddress [Email],
	P.PhoneNumber [Phone],
	P.IsSalesperson [SalesPerson],
	JSON_VALUE(CustomFields, '$.Title') [CurrentJob.Title],
	JSON_VALUE(CustomFields, '$.PrimarySalesTerritory') [CurrentJob.Territory],
	JSON_VALUE(CustomFields, '$.HireDate') [CurrentJob.Hire Date]

FROM
	Application.People P
WHERE
	P.isEmployee = 1
FOR JSON PATH

--SELECT CustomFields FROM Application.People FOR JSON PATH
/*
In MongoDB, create a database called WideWorldImporters 
	and a collection called Employees.  Use the results of 
	your query (the JSON string) and insert the records into
	the newly created Employees table.

Create a MongoDB query to return all the records you just
	created in the Employees table.

Next, write a MongoDB query that returns just Piper Koch's record.

Next, write a MongoDB query that updates Piper's title to "Senior Manager".

Last, write a MongoDB query that will remove Amy Trefl's record from the
	database

Turn in the .js file for all the above commands
*/

