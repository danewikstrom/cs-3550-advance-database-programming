/* If you need to start over, execute these */
--ALTER TABLE EmployeeSalaries DROP CONSTRAINT fk_EmployeeKey
--DROP TABLE EmployeeSalaries
--ALTER TABLE Employees DROP CONSTRAINT fk_DepartmentKey
--DROP TABLE Employees
--DROP TABLE Departments
/*End of start over script */


/*
Dane Wikstrom
CS 3550
Final - Part 2
*/




--USE CS3550Final

CREATE TABLE Employees
(
	EmployeeKey int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	FirstName varchar(255) NOT NULL,
	LastName varchar(255) NOT NULL,
	SupervisorEmployeeKey int,
	DepartmentKey int NOT NULL,
	Hired date NOT NULL,
	BirthDate date NOT NULL,
	Email varchar(255) NOT NULL,
	Active bit DEFAULT(1)
)

CREATE TABLE Departments
(
	DepartmentKey int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	DepartmentName varchar(255),
	Active bit DEFAULT(1)
)

CREATE TABLE EmployeeSalaries
(
	EmployeeSalaryKey int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	EmployeeKey int NOT NULL, 
	Salary money NOT NULL,
	StartDate date NOT NULL,
	EndDate date NULL
)

ALTER TABLE Employees ADD CONSTRAINT fk_DepartmentKey FOREIGN KEY (DepartmentKey) REFERENCES Departments(DepartmentKey)
ALTER TABLE EmployeeSalaries ADD CONSTRAINT fk_EmployeeKey FOREIGN KEY (EmployeeKey) REFERENCES Employees(EmployeeKey)

SET IDENTITY_INSERT Departments ON
INSERT Departments (DepartmentKey, DepartmentName) VALUES (1, 'CEO')
INSERT Departments (DepartmentKey, DepartmentName) VALUES (2, 'Operations')
INSERT Departments (DepartmentKey, DepartmentName) VALUES (3, 'Sales')
INSERT Departments (DepartmentKey, DepartmentName) VALUES (4, 'Information Technology')
INSERT Departments (DepartmentKey, DepartmentName) VALUES (5, 'Business Intelligence')
SET IDENTITY_INSERT Departments OFF

SET IDENTITY_INSERT Employees ON
INSERT Employees (EmployeeKey, SupervisorEmployeeKey, FirstName, LastName, DepartmentKey, Hired, BirthDate, Email) VALUES (1, NULL, 'Da', 'Boss', 1, '12/25/2010', '06/15/1968', 'DBoss@gmail.com')
INSERT Employees (EmployeeKey, SupervisorEmployeeKey, FirstName, LastName, DepartmentKey, Hired, BirthDate, Email) VALUES (2, 5, 'A', 'Grunt', 4, '1/15/2011', '11/05/1992', 'AGrunt@gmail.com')
INSERT Employees (EmployeeKey, SupervisorEmployeeKey, FirstName, LastName, DepartmentKey, Hired, BirthDate, Email) VALUES (3, 5, 'Number', 'Cruncher', 5, '1/16/2011', '12/12/1991', 'NCruncher@gmail.com')
INSERT Employees (EmployeeKey, SupervisorEmployeeKey, FirstName, LastName, DepartmentKey, Hired, BirthDate, Email) VALUES (4, 1, 'Slick', 'Deal', 3, '12/25/2010', '09/18/1975', 'SDeal@gmail.com')
INSERT Employees (EmployeeKey, SupervisorEmployeeKey, FirstName, LastName, DepartmentKey, Hired, BirthDate, Email) VALUES (5, 1, 'Doit', 'Thisway', 2, '3/01/2011', '5/16/1999', 'DThisway@gmail.com')
INSERT Employees (EmployeeKey, SupervisorEmployeeKey, FirstName, LastName, DepartmentKey, Hired, BirthDate, Email) VALUES (6, 4, 'Closea', 'Deal', 3, '06/01/2011', '09/11/1988', 'CDeal@gmail.com')
INSERT Employees (EmployeeKey, SupervisorEmployeeKey, FirstName, LastName, DepartmentKey, Hired, BirthDate, Email) VALUES (7, 2, 'FixMy', 'Computer', 4, '08/15/2012', '11/13/1998', 'FComputer@gmail.com')
INSERT Employees (EmployeeKey, SupervisorEmployeeKey, FirstName, LastName, DepartmentKey, Hired, BirthDate, Email) VALUES (8, 5, 'Doit', 'Thatway', 2, '10/15/2012', '10/28/1995', 'DThatway@gmail.com')
SET IDENTITY_INSERT Employees OFF

INSERT EmployeeSalaries (EmployeeKey, Salary, StartDate, EndDate) VALUES (1, 100000, '12/25/2010', '12/31/2011')
INSERT EmployeeSalaries (EmployeeKey, Salary, StartDate, EndDate) VALUES (1, 150000, '1/1/2012', NULL)
INSERT EmployeeSalaries (EmployeeKey, Salary, StartDate, EndDate) VALUES (2, 47500, '1/15/2011', NULL)
INSERT EmployeeSalaries (EmployeeKey, Salary, StartDate, EndDate) VALUES (3, 47500, '1/16/2011', '1/15/2012')
INSERT EmployeeSalaries (EmployeeKey, Salary, StartDate, EndDate) VALUES (3, 47500, '1/16/2012', NULL)
INSERT EmployeeSalaries (EmployeeKey, Salary, StartDate, EndDate) VALUES (4, 90000, '12/25/2010', '12/31/2011')
INSERT EmployeeSalaries (EmployeeKey, Salary, StartDate, EndDate) VALUES (4, 120000, '1/1/2012', NULL)
INSERT EmployeeSalaries (EmployeeKey, Salary, StartDate, EndDate) VALUES (5, 55000, '3/1/2011', NULL)
INSERT EmployeeSalaries (EmployeeKey, Salary, StartDate, EndDate) VALUES (6, 90000, '6/1/2011', NULL)
INSERT EmployeeSalaries (EmployeeKey, Salary, StartDate, EndDate) VALUES (7, 47500, '8/15/2012', NULL)
INSERT EmployeeSalaries (EmployeeKey, Salary, StartDate, EndDate) VALUES (8, 50000, '10/15/2012', NULL)


SELECT * FROM Employees
SELECT * FROM Departments
SELECT * FROM EmployeeSalaries

/*
Information about database - 

	Simple primary/foreign key relationship between Employees and Departments.
		Another primary/foreign key relationship between Employees and EmployeeSalaries.
	
	On the Employees table, the SupervisorEmployeeKey references the EmployeeKey
		in the Employees table.  The top of the hierarchy will not have a 
		value in this field - everyone else will.
	
	EmployeeSalaries keeps the history of all the salaries an employee has had.  When
		the salary changes, a date is put in EndDate column and a new record is created 
		for the new salary - with a StartDate for the next day.  This can be seen with
		employee #1 - they made 100k from December 25, 2010 through December 31, 2011.
		They then have a new record with a start date of January 1, 2012 (with a NULL end
		date - meaning it is their active salary).

	Remember to preface all objects (stored procedures, views, triggers, etc.) with your
	first initial and full last name (rreed_myView as an example).
*/



/* 
Create a view that shows all employees.  Include first and last name 
in the format of lastname, firstname (Reed, Russell), email address, 
department, hire date, birthdate (with the year missing or x'd out - 11/05 or 11/05/xxxx)
and salary.  Also include their supervisor's name (in the same
format mentioned above).

Only return active records.

You should get 8 rows back when you select * from your view.
*/

GO

CREATE VIEW dbo.dwikstrom_employeeView 
AS
SELECT 
	CONCAT(e.LastName,', ',e.FirstName) [Full Name],
	e.Email, 
	d.DepartmentName [Department],
	e.Hired [Date Hired],
	FORMAT(e.BirthDate,'MM/dd/XXXX') [Birthdate],
	es.Salary,
	EmpSup.[Supervisor]

FROM 
	Employees e
	INNER JOIN
	(
	SELECT DISTINCT
		CONCAT(A.LastName,', ',A.FirstName) [Supervisor]
	FROM 
		Employees A, Employees B
	WHERE 
		A.EmployeeKey <> B.DepartmentKey AND A.EmployeeKey = B.SupervisorEmployeeKey 
	) EmpSup ON CONCAT(e.LastName,', ',e.FirstName) = EmpSup.Supervisor
	INNER JOIN EmployeeSalaries es ON es.EmployeeKey = e.EmployeeKey
	INNER JOIN Departments d ON d.DepartmentKey = e.DepartmentKey
WHERE
	e.Active = 1 AND d.Active = 1
GO

SELECT * FROM dbo.dwikstrom_employeeView

DROP VIEW dbo.dwikstrom_employeeView	
	
/*
Laws enacted by Obama in 2016 set a minimum salary for Exempt employees
	at $47,500 (and everyone in this company is exempt from overtime!).  
	Corporate governance also limits salaries to a maximum of $250,000.
	Add a constraint to the EmployeeSalaries table to make sure these
	two rules are enforced.
*/

ALTER TABLE EmployeeSalaries
ADD CONSTRAINT salaryLimit CHECK (Salary BETWEEN 47500 AND 250000)

/*
Create a stored procedure to insert a new employee, including their
	starting salary.

For this exercise, assume your application would know the key values 
	for department and supervisor.  Make sure all the fields in 
	the Employees table are populated (either with defaults or
	values passed into your stored procedure)

Also include a script that executes your stored procedure -
	EXEC yourStoredProc value1, value2, 'date', etc.

Make sure to account for values that will cause your constraint to fail
	entering in your salary.  You want to avoid the employee record getting
	added without a cooresponding record in the EmployeeSalaries table.
	You have a number of options for this - TRY/CATCH, Transactions, 
	TSQL Programming.  For Transactions, I'd lookup "SET XACT_ABORT ON"

Something of value to lookup - SCOPE_IDENTITY() function.

*/
GO
CREATE PROCEDURE dbo.dwikstrom_procInsertNewEmployee
	@employeeKey int,
	@supervisorEmployeeKey int,
	@firstName varchar(255),
	@lastName varchar(255),
	@departmentKey int,
	@hireDate date,
	@birthDate date,
	@email varchar(255),
	@salary money
AS
BEGIN
	INSERT INTO Employees(
		[EmployeeKey], 
		[SupervisorEmployeeKey], 
		[FirstName], 
		[LastName], 
		[DepartmentKey], 
		[Hired], 
		[BirthDate], 
		[Email]) 
	VALUES (
		@employeeKey, 
		@supervisorEmployeeKey, 
		@firstName, 
		@lastName, 
		@departmentKey, 
		@hireDate, 
		@birthDate, 
		@email)
	IF(@employeeKey IS NOT NULL AND --make sure that fields in the employee table have been filled
		@supervisorEmployeeKey IS NOT NULL AND
		@firstName IS NOT NULL AND 
		@lastName IS NOT NULL AND 
		@departmentKey IS NOT NULL AND 
		@hireDate IS NOT NULL AND 
		@birthDate IS NOT NULL AND 
		@email IS NOT NULL)
		BEGIN
			INSERT INTO EmployeeSalaries(
				[Salary]) 
			VALUES(
				@salary)
		END
END

EXEC dbo.dwikstrom_procInsertNewEmployee 
	12, 
	5, 
	'Dane', 
	'Wikstrom', 
	4, 
	'08/13/2017', 
	'12/31/1989', 
	'danewikstrom@mail.weber.edu',
	59000

DROP PROCEDURE dbo.dwikstrom_procInsertNewEmployee

/*
Create a trigger that prevents the deletion of an employee 
	and instead sets the active bit to 0.  Make sure it
	works on batches (DELETE Employees WHERE DepartmentKey = 4)

*/
GO

CREATE TRIGGER dbo.dwikstrom_onDeleteEmployee
ON Employees
INSTEAD OF DELETE
AS
BEGIN
	UPDATE e SET Active = 0
	FROM 
		deleted d
		INNER JOIN Employees e ON d.DepartmentKey = e.DepartmentKey
END

GO

DELETE 
	Employees 
WHERE 
	DepartmentKey = 4

SELECT 
	* 
FROM 
	Employees
 
DROP TRIGGER dwikstrom_onDeleteEmployee

/* 
Create a stored prodedure to update someone's salary.  Account for 
	the rules described in constraint (salary can't be less than 47,500
	and greater than 250k).  You also need to make sure that the start date
	of the new salary is not before the current salary.  Example on this - 
	if an employee has a record that looks like -

	EmployeeSalaryKey	EmployeeKey		Salary		StartDate	EndDate
	3					2				47500.00	2011-01-15	NULL

	For this record, the stored procedure should reject a new salary with a start 
	date on or before 1/15/2011.  The earlies the new salary could start is 1/16/2011.

Your stored procedure should accept an employee key, start date of the 
	new salary, and the new salary.  You can assume that there is only
	one active salary (and there is always an active salary - a row with no end date).

Include three execution statements for your stored procedure - one that updates a salary
	to a valid number, one that tries to update a salary to something outside the acceptable
	range, and one that tries to make a new salary for a date that is earlier than 
	the current record.

*/
GO

CREATE PROCEDURE dbo.dwikstrom_procUpdateASalary
	@employeeKey int = 9,
	@salaryStartDate date = '12/31/2017',
	@salary money = 55000
AS
	SELECT 
		es.EmployeeSalaryKey,
		@employeeKey,
		@salary,
		@salaryStartDate,
		es.EndDate
	FROM
		Employees e,
		EmployeeSalaries es


EXEC dbo.dwikstrom_procUpdateASalary @salary = 60000
EXEC dbo.dwikstrom_procUpdateASalary @salary = 500000
EXEC dbo.dwikstrom_procUpdateASalary @salaryStartDate = '12/31/1989'

GO

DROP PROCEDURE dwikstrom_procUpdateASalary
/*
Create a function that calculates a salary increase.  Your function
	should accept the amount to increase the salary and the current
	salary.  Account for the limits described in previous questions (47500 
	and 250k)

Include a select statement that leverages your new function to give
	everyone but the CEO a 10% increase.
*/

GO

CREATE FUNCTION dbo.dwikstrom_salaryIncreaseCalculator(@salaryIncreasePercentage money, @currentSalary money)
RETURNS money 
AS
BEGIN
	IF(@salaryIncreasePercentage + @currentSalary > 250000)
	BEGIN
		RETURN @currentSalary
	END

	IF(@currentSalary < 47500)
	BEGIN
		RETURN @currentSalary
	END

	DECLARE @increase decimal
	SET @increase = (@salaryIncreasePercentage * @currentSalary)
	RETURN @increase
END
GO

DECLARE @salIncrease money
SET @salIncrease = 0.1

SELECT 
	dbo.dwikstrom_salaryIncreaseCalculator(@salIncrease, es.Salary)[Salary Increase],
	e.FirstName,
	e.LastName,
	d.DepartmentName
FROM
	EmployeeSalaries es
	INNER JOIN
	Employees e ON e.EmployeeKey = es.EmployeeKey
	INNER JOIN
	Departments d ON d.DepartmentKey = e.DepartmentKey
WHERE
	d.DepartmentKey != 1 AND d.DepartmentName != 'CEO' AND e.FirstName != 'Da' AND e.LastName != 'Boss'

DROP FUNCTION dwikstrom_salaryIncreaseCalculator