/*
Dane Wikstrom
CS 3550
Final Part 1: Database Design Script
*/

CREATE TABLE [Collections] (
  [CollectionID]	int		NOT NULL,
  [CollectionName]	varchar(75)		NOT NULL,
  [InProduction]	bit		NOT NULL,
  PRIMARY KEY ([CollectionID])
);


CREATE TABLE [Sets] (
  [SetID]	int		NOT NULL,
  [CollectionID]	int		NOT NULL,
  [SetName]		varchar(75)		NOT NULL,
  [SetNumOfPieces]		int		NOT NULL,
  [SetYearReleased]		date	NOT NULL,
  [InProduction]	bit		NOT NULL,
  PRIMARY KEY ([SetID])
);

CREATE INDEX [FK] ON  [Sets] ([CollectionID]);


CREATE TABLE [SetPieces] (
  [SetPieceID]	int		NOT NULL,
  [SetID]	int		NOT NULL,
  [PieceID]		int		NOT NULL,
  [SetPieceQTY]		int		NOT NULL,
  PRIMARY KEY ([SetPieceID])
);

CREATE INDEX [FK] ON  [SetPieces] ([SetID], [PieceID]);


CREATE TABLE [Pieces] (
  [PieceID]		int		NOT NULL,
  [ShapeID]		int		NOT NULL,
  [ColorID]		int		NOT NULL,
  [CategoryID]	int		NOT NULL,
  PRIMARY KEY ([PieceID])
);

CREATE INDEX [FK] ON  [Pieces] ([ShapeID], [ColorID], [CategoryID]);

CREATE TABLE [Colors] (
  [ColorHexNumber]	int		NOT NULL,
  [ColorRGBCode]	varchar(20)		NOT NULL,
  [ColorName]	varchar(35)		NOT NULL,
  PRIMARY KEY ([ColorHexNumber])
);


CREATE TABLE [Shapes] (
  [ShapeID]		int		NOT NULL,
  [ShapeName]	varchar(35)		NOT NULL,
  [ShapeDescription]	 varchar(2000)		NOT NULL,
  PRIMARY KEY ([ShapeID])
);


CREATE TABLE [PieceCategories] (
  [CategoryID]	int		NOT NULL,
  [CategoryName]	varchar(75)		NOT NULL,
  [isFigurePiece]	bit		NOT NULL,
  PRIMARY KEY ([CategoryID])
);


CREATE TABLE [MiniFigurePieces] (
  [MiniFigurePieceID]	int		NOT NULL,
  [CategoryID]	int		NOT NULL,
  [MiniFigureID]	int		NOT NULL,
  PRIMARY KEY ([MiniFigurePieceID])
);

CREATE INDEX [FK] ON  [MiniFigurePieces] ([CategoryID], [MiniFigureID]);


CREATE TABLE [MiniFigures] (
  [MiniFigureID]	int		NOT NULL,
  [MiniFigureName]	varchar(75)		NOT NULL,
  [TorsoID]		int		NOT NULL,
  [LegID]	int		NOT NULL,
  [HeadID]	int		NOT NULL,
  [HeadAccessoryID]		int		NULL,
  [InProduction]	bit		NOT NULL,
  PRIMARY KEY ([MiniFigureID])
);


CREATE TABLE [MiniFigureAccessoryPeices] (
  [AccessoryID]		int		NOT NULL,
  [AccessoryName]	varchar(75)		NOT NULL,
  PRIMARY KEY ([AccessoryID])
);




