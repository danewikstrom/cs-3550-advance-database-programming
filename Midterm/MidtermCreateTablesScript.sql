/*
Dane Wikstrom
CS 3550
CREATE TABLE Scipts for Midterm
*/


--Employees Table
CREATE TABLE [Employees] 
(
  [EmployeeID]									int				NOT NULL,
  [DepartmentID]								int				NOT NULL,
  [JobTitleID]									int				NOT NULL,
  [SupervisorID]								int				NOT NULL,
  [ComputerID]									int				NULL,
  [EmployeeFirstName]							varchar(35)		NOT NULL,
  [EmployeeLastName]							varchar(35)		NOT NULL,
  [EmployeeEmail]								varchar(255)	NOT NULL,
  [EmployeeActive]								bit				NOT NULL,
  PRIMARY KEY ([EmployeeID])
);

CREATE INDEX [FK] 
	ON  [Employees] 
	([DepartmentID], [JobTitleID], [SupervisorID], [ComputerID]);

GO

--JobTitles Table
CREATE TABLE [JobTitles] 
(
  [JobTitleID]									int				NOT NULL,
  [EmployeeID]									int				NOT NULL,
  [JobTitleName]								varchar(70)		NOT NULL,
  [JobTitleActive]								bit				NOT NULL,
  PRIMARY KEY ([JobTitleID])
);

CREATE INDEX [FK] 
	ON  [JobTitles] 
	([EmployeeID]);

GO

--EmployeeJobHistory Table
CREATE TABLE [EmployeeJobHistory] 
(
  [EmployeeID]									int				NOT NULL,
  [JobTitleID]									int				NOT NULL,
  [DepartmentID]								int				NOT NULL,
  [EmployeeJobHistoryHireDate]					date			NOT NULL,
  [EmployeeJobHistoryTerminationDate]			date			NULL
);

CREATE INDEX [PK, FK] 
	ON  [EmployeeJobHistory] 
	([EmployeeID]);

CREATE INDEX [FK] 
	ON  [EmployeeJobHistory] 
	([JobTitleID], [DepartmentID]);

GO

--Departments Table
CREATE TABLE [Departments] 
(
  [DepartmentID]								int				NOT NULL,
  [DepartmentContactID]							int				NOT NULL,
  [DepartmentName]								varchar(70)		NOT NULL,
  [DepartmentActive]							bit				NOT NULL,
  PRIMARY KEY ([DepartmentID])
);

CREATE INDEX [FK] 
	ON  [Departments] 
	([DepartmentContactID]);

GO

--DepartmentContacts Table
CREATE TABLE [DepartmentContacts] 
(
  [DepartmentContactID]							int				NOT NULL,
  [DepartmentID]								int				NOT NULL,
  [DepartmentContactFirstName]					varchar(35)		NOT NULL,
  [DepartmentContactLastName]					varchar(35)		NOT NULL,
  [DepartmentContactPhoneNumber]				varchar(35)		NOT NULL,
  [DepartmentContactEmail]						varchar(255)	NOT NULL,
  [DepartmentContacActive]						bit				NOT NULL,
  PRIMARY KEY ([DepartmentContactID])
);

CREATE INDEX [FK] 
	ON  [DepartmentContacts] 
	([DepartmentID]);

GO

--Supervisors Table
CREATE TABLE [Supervisors] 
(
  [SupervisorID]								int				NOT NULL,
  [EmployeeID]									int				NOT NULL,
  [DepartmentID]								int				NOT NULL,
  [SupervisorActive]							bit				NOT NULL,
  PRIMARY KEY ([SupervisorID])
);

CREATE INDEX [FK] 
	ON  [Supervisors] 
	([EmployeeID], [DepartmentID]);

GO

--Computers Table
CREATE TABLE [Computers] 
(
  [ComputerID]									int				NOT NULL,
  [ComputerSpecsID]								int				NOT NULL,
  [EmployeeID]									int				NOT NULL,
  [ComputerActive]								bit				NOT NULL,
  PRIMARY KEY ([ComputerID])
);

CREATE INDEX [FK] 
	ON  [Computers] 
	([ComputerSpecsID], [EmployeeID]);

GO

--ComputerSpecs Table
CREATE TABLE [ComputerSpecs] 
(
  [ComputerID]									int				NOT NULL,
  [ComputerSpecCost]							money			NOT NULL,
  [ComputerSpecPurchaseDate]					date			NOT NULL,
  [ComputerSpecBrand]							varchar(35)		NOT NULL,
  [ComputerSpecMemoryAmount]					varchar(15)		NOT NULL,
  [ComputerSpecHDSpace]							varchar(15)		NOT NULL,
  [ComputerSpecCPUType]							varchar(35)		NOT NULL,
  [ComputerSpecVideoCardType]					varchar(35)		NOT NULL
);

CREATE INDEX [PK, FK] 
	ON  [ComputerSpecs] 
	([ComputerID]);

GO

--ComputerHistory Table
CREATE TABLE [ComputerHistory] 
(
  [ComputerID]									int				NOT NULL,
  [ComputerHistoryEmployeeStartDate]			date			NOT NULL,
  [ComputerHistoryEmployeeEndDate]				date			NULL,
  [ComputerHistoryEndOfUsefulLifeDate]			date			NULL
);

CREATE INDEX [PK, FK] 
	ON  [ComputerHistory] 
	([ComputerID]);