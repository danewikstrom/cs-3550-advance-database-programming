/*
Dane Wikstrom
CS 3550
Midterm Queries
*/

USE WideWorldImporters

GO

/*
(10 points) Write a query that returns the total sales in 2015 for the Stock Item �USB food flash drive � donut�.  
Leverage the extended price column for the calculation and the invoice date to determine what year the product was sold (Invoice tables).
You should get $74,372.80
*/

Select 
	'$' + CONVERT(varchar(20), CAST(SUM(IL.ExtendedPrice) AS money), 1 )  AS [Total Sales for 2015]
FROM 
	Sales.InvoiceLines IL
	INNER JOIN Sales.Invoices I
	ON IL.InvoiceID = I.InvoiceID
WHERE 
	IL.StockItemID = 12 AND
	YEAR(I.InvoiceDate) = 2015

GO

/*
(15 points) Write a query that returns how many customers we have in each state.  
Customers can be found in the Sales.Customers database, city and state information in the Application.StateProvinces and Application.Cities tables.  
Use the DeliveryCityID column in Customers to make your joins.  
Your query should return all states � 53 records and should be ordered from the most to the least number of customers.  
Return the full state name, not the two-letter code
Delaware has 0 customers, Texas has 46
*/

SELECT
	SP.StateProvinceName AS [State / Province],
	COUNT(Cust.CustomerName) AS [Customers Per State]
FROM
	Sales.Customers Cust
		FULL JOIN Application.Cities Cit
		ON Cust.DeliveryCityID = Cit.CityID
		FULL JOIN Application.StateProvinces SP
		ON SP.StateProvinceID = Cit.StateProvinceID
GROUP BY
	SP.StateProvinceName
ORDER BY
	COUNT(Cust.CustomerName) ASC

GO

/*
(15 points) Write a query to return a list of people with duplicated email addresses in the Application.People table.  
Your query needs to return the person id, first name and last name (in separate columns based on the full name column), email address and phone number.
Sort the list by email address and last name.

158 records should be returned
Hint � you need to first identify the duplicate email addresses, then join these results back into the People table (derived table).
One of the duplicate emails - teresa@wingtiptoys.com
*/

SELECT 
	P.PersonID,
	RTRIM (SUBSTRING (P.FullName, 1, CHARINDEX (' ', P.FullName) ) ) AS [First Name],
	LTRIM (SUBSTRING (P.FullName, CHARINDEX(' ', P.FullName), LEN(P.FullName) ) )AS [Last Name], 
	P.EmailAddress,
	P.PhoneNumber
FROM
	Application.People p

INNER JOIN

(
SELECT
	P.EmailAddress, 
	COUNT(P.EmailAddress) AS [NumOfEmailAddresses]
FROM
	Application.People P
GROUP BY
	P.EmailAddress
HAVING 
	COUNT(P.EmailAddress) > 1
) duplicates
ON duplicates.EmailAddress = P.EmailAddress
