﻿/*
Assignment #6: T-SQL Programming & JSON

Dane Wikstrom
CS 3550 - Summer 2017
*/

/*
Question #1 - 15 points - Find all prime numbers between 1 and 10,000.  Using T-SQL, 
	code something that prints to the message window (using PRINT) any number between 1 and 10k that is 
	prime.

Use good code formatting - indentation and whitespace.  Your algorithm can be as sophisticated as you 
	want it to be (don't have to make this super efficient).  A list of prime numbers can be
	found at https://primes.utm.edu/lists/small/100000.txt
*/

USE WideWorldImporters



--Using Primality Test 
/*
If a number is a prime number, then it must not be divisible by a prime that is less than or equal to the ceiling of the square root of that number. 
The square root of 10,000 is 100. The largest prime number less than 100 is 97. Therefore, if the number being tested %(modulo) up to 97 = 0, then the number is not prime.
*/


DECLARE @num int
SET @num = 0

WHILE (@num < 10000)
BEGIN
SET @num = @num + 1

	IF(@num = 1)-- 1 is not prime
	BEGIN
		CONTINUE
    END 

	IF ( @Num % 2 = 0) -- no even numbers are prime
    BEGIN
		CONTINUE
	END

	IF ((@Num > 3) AND (@Num % 3 = 0)) --not prime
    BEGIN
		CONTINUE
	END

	IF ((@Num > 5) AND (@Num % 5 = 0)) -- not prime
    BEGIN
		CONTINUE
	END

	IF ((@Num > 7) AND (@Num % 7 = 0)) -- not prime
    BEGIN
		CONTINUE
	END

	IF( (@Num > 11) AND (@num % 11 = 0))--not prime
	BEGIN
		CONTINUE
    END

	IF( (@Num > 13) AND (@num % 13 = 0))--not prime
	BEGIN
		CONTINUE
    END

	IF ((@Num > 17) AND (@Num % 17 = 0)) -- not prime
    BEGIN
		CONTINUE
	END

	IF( (@Num > 19) AND (@num % 19 = 0))--not prime
	BEGIN
		CONTINUE
    END

	IF( (@Num > 23) AND (@num % 23 = 0))--not prime
	BEGIN
		CONTINUE
    END

	IF ((@Num > 29) AND (@Num % 29 = 0)) -- not prime
    BEGIN
		CONTINUE
	END

	IF( (@Num > 31) AND (@num % 31 = 0))--not prime
	BEGIN
		CONTINUE
    END

	IF ((@Num > 37) AND (@Num % 37 = 0)) --not prime
    BEGIN
		CONTINUE
	END

	IF ((@Num > 41) AND (@Num % 41 = 0)) -- not prime
    BEGIN
		CONTINUE
	END

	IF ((@Num > 43) AND (@Num % 43 = 0)) -- not prime
    BEGIN
		CONTINUE
	END

	IF( (@Num > 47) AND (@num % 47 = 0))--not prime
	BEGIN
		CONTINUE
    END

	IF( (@Num > 53) AND (@num % 53 = 0))--not prime
	BEGIN
		CONTINUE
    END

	IF ((@Num > 59) AND (@Num % 59 = 0)) -- not prime
    BEGIN
		CONTINUE
	END

	IF( (@Num > 61) AND (@num % 61 = 0))--not prime
	BEGIN
		CONTINUE
    END

	IF( (@Num > 67) AND (@num % 67 = 0))--not prime
	BEGIN
		CONTINUE
    END

	IF ((@Num > 71) AND (@Num % 71 = 0)) -- not prime
    BEGIN
		CONTINUE
	END

	IF( (@Num > 73) AND (@num % 73 = 0))--not prime
	BEGIN
		CONTINUE
    END

	IF ((@Num > 79) AND (@Num % 79 = 0)) --not prime
    BEGIN
		CONTINUE
	END
	IF ((@Num > 83) AND (@Num % 83 = 0)) -- not prime
    BEGIN
		CONTINUE
	END

	IF ((@Num > 89) AND (@Num % 89 = 0)) -- not prime
    BEGIN
		CONTINUE
	END

	IF( (@Num > 97) AND (@num % 97 = 0))--not prime
	BEGIN
		CONTINUE
    END

	ELSE --else then the number is prime
	BEGIN
		PRINT @num
	END
END


/* 
Question #2 - 10 points - Using much of the work done in question #1, create a function that
	accepts a number and returns a bit indicating if the passed in number was prime.
	1 for yes, 0 for no.  Call your fuction dbo.firstinitialwholelastname_isPrime (dbo.rreed_isPrime)
*/



--using Primality test 
/*
Pseudocode

function is_prime(n)
    if n ≤ 1
        return false
    else if n ≤ 3
        return true
    else if n mod 2 = 0 or n mod 3 = 0
        return false
    let i = 5
    while i * i ≤ n
        if n mod i = 0 or n mod (i + 2) = 0
            return false
        i = i + 6
    return true
*/



--DROP FUNCTION dbo.dwikstrom_isPrime;

GO
CREATE FUNCTION dbo.dwikstrom_isPrime (@number int)
RETURNS bit
BEGIN
    DECLARE @isTheNumPrime bit
    DECLARE @i int
	SET @number = 5
    SET @isTheNumPrime = 1
    SET @i = 5

	IF(@number <= 1)-- 1 and negatives are not prime
	BEGIN
        SET @isTheNumPrime = 0--number is not prime
        RETURN @isTheNumPrime
    END 

	IF(@number <= 3)--2 or 3
	BEGIN
        SET @isTheNumPrime = 1--number is prime
        RETURN @isTheNumPrime
    END

	IF(@number % 2 = 0 OR @number % 3 = 0)
	BEGIN
        SET @isTheNumPrime = 0--number is not prime
        RETURN @isTheNumPrime
    END

    WHILE (@i * @i <= @number)
    BEGIN

        IF ((@number % @i = 0) OR (@number % (@i + 2) = 0))
        BEGIN
            SET @isTheNumPrime = 0--number is not prime
            BREAK
        END

        SET @i = @i + 6
    END
	SET @isTheNumPrime = 1
    RETURN @isTheNumPrime
END


/*

Question #3 - 15 points - Create a function that accepts a quantity, unit price
	and tax rate.  The function should first calculate the total value of
	the information passed in - (quantity * unit price) * 1+taxrate.
	If there are more than 100 items purchased, give a discount of 8%.  If
	more than 250 item were purchased, give a 12% discount

Return the final value.  Use the Sales.OrderLines records to test
	your function.  As outlined earlier in class, name your function with 
	your first initial and full last name (dbo.rreed_CalculateDiscount)
*/


--DROP FUNCTION dbo.dwikstrom_CalculateDiscount;
GO
CREATE FUNCTION dbo.dwikstrom_CalculateDiscount(@quantity int, @unitPrice decimal, @taxRate decimal)
RETURNS decimal
BEGIN
	DECLARE @finalValue decimal
	DECLARE @totalValue decimal

	SET @quantity = 10
	SET @unitPrice = 10.0
	SET @taxRate = .782
	SET @totalValue = (@quantity * @unitPrice) * (1 + @taxRate)
	SET @finalValue = @totalValue

	IF(@quantity > 100) --if quanity is greater than 100
	BEGIN
		SET @finalValue = (@totalValue - (@totalValue * .08))--give an 8% discount
	END

	IF(@quantity > 250)--if quanity is greater than 250
	BEGIN
		SET @finalValue = (@totalValue - (@totalValue * .12))--give a 12% discount
	END
	RETURN @finalValue
END
GO

SELECT
	dbo.dwikstrom_CalculateDiscount(OL.Quantity, OL.UnitPrice, OL.TaxRate)
FROM
	Sales.OrderLines OL


/*

Question #4 - 10 points - Write a query that exports a JSON string for customerID 200
	(Tailspin Toys in Tooele, UT).  The query should generate the below string exactly.
	Tables that you need to use - Sales.Customers, Sales.CustomerCategories, Application.Citites
	Application.StateProvince, and Application.People.  I'm using the delivery address
	information (including deliveryCityID to join on)
*/	



--I wan't 100% sure on if you were looking for something like number 1 or number 2 so I left both



------NUMBER 1

SELECT
	C.CustomerID [CustomerID],
	C.CustomerName [CustomerName],
	C.PrimaryContactPersonID [CustomerContact.PersonID],
	LEFT(P.FullName, CHARINDEX(' ', P.FullName) ) [CustomerContact.FirstName],
	LTRIM(SUBSTRING(P.FullName, CHARINDEX(' ', P.FullName), LEN(P.FullName)) ) [CustomerContact.LastName],
	P.EmailAddress [CustomerContact.Email],
	C.DeliveryAddressLine2 [CustomerContact.Address1],
	C.DeliveryAddressLine1 [CustomerContact.Address2],
	CI.CityName [CustomerContact.CityName],
	SP.StateProvinceName [CustomerContact.State],
	C.DeliveryPostalCode [CustomerContact.Zip],
	CC.CustomerCategoryName [CustomerType],
	C.WebsiteURL [Website]

FROM
	Sales.Customers C
	FULL OUTER JOIN Sales.CustomerCategories CC ON C.CustomerCategoryID = CC.CustomerCategoryID
	FULL OUTER JOIN Application.Cities CI ON CI.CityID = C.DeliveryCityID
	FULL OUTER JOIN Application.StateProvinces SP ON SP.StateProvinceID = CI.StateProvinceID
	FULL OUTER JOIN Application.People P ON C.BillToCustomerID = P.PersonID

WHERE
	C.CustomerID = 200 
	
FOR JSON PATH


------NUMBER 2

DECLARE @json nvarchar(max) = 
'[
	{
		"CustomerID":200,
		"CustomerName":"Tailspin Toys (Tooele, UT)",
		"CustomerContact":
		{
			"PersonID":1399,
			"FirstName":"Fanni",
			"LastName": "Benko",
			"Email":"fanni@tailspintoys.com",
			"Address1":"691 Sonkar Avenue",
			"Address2":"Suite 264",
			"City": "Tooele",
			"State": "Utah",
			"Zip":"90100"
		},
		"CustomerType":"Novelty Shop",
		"Website":"http:\/\/www.tailspintoys.com\/Tooele"
	}
]'

SELECT
	X.CustomerID,
	X.CustomerName,
	X.PersonID,
	X.FirstName,
	X.LastName,
	X.Email,
	X.Address1,
	X.Address2,
	X.City,
	X.[State],
	X.Zip,
	X.CustomerType,
	X.Website,
	JSON_VALUE(X.CustomerContactObject, '$.CustomerContact'),
	X.CustomerContactObject
FROM
	OPENJSON(@json)
		WITH (
			CustomerID int '$.CustomerID',
			CustomerName nvarchar(max) '$.CustomerName',
			PersonID int '$.CustomerContact.PersonID',
			FirstName nvarchar(max) '$.CustomerContact.FirstName',
			LastName nvarchar(max) '$.CustomerContact.LastName',
			Email nvarchar(max) '$.CustomerContact.Email',
			Address1 nvarchar(max) '$.CustomerContact.Address1',
			Address2 nvarchar(max) '$.CustomerContact.Address2',
			City nvarchar(max) '$.CustomerContact.City',
			[State] nvarchar(max) '$.CustomerContact.State',
			Zip nvarchar(max) '$.CustomerContact.Zip',				
			CustomerType nvarchar(max) '$.CustomerType',
			Website nvarchar(max) '$.Website',
				CustomerContactObject nvarchar(max) '$.CustomerContact' AS JSON --Required if the return is a JSON string/object
			) X


